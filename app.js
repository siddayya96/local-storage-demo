const express = require('express');
const app = express();
const port = process.env.PORT || 8000;
let options = {
    dotfiles: "ignore",
    etag: true,
    extentions: ["htm", "html"],
    redirect: false,
    setHeaders: function (res, path, stat) {
        res.set("x-timestamp", Date.now());
    }
};
app.use(express.static('./', options));

app.listen(port, console.log("server running on port " +port));